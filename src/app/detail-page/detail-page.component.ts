import { Component, OnInit, Input } from '@angular/core';
import { Beer } from '../beer';

import { SharingService } from '../sharing-data.service';
import { BeersService } from '../beers.service';

@Component({
  selector: 'app-detail-page',
  templateUrl: './detail-page.component.html',
  styleUrls: ['./detail-page.component.css']
})
export class DetailPageComponent implements OnInit {


  similarBeers: Array<Beer> = [];
  @Input('beer') beer: Beer;
  constructor(
    private sharingData: SharingService,
    private beersService: BeersService,
  ) { }

  ngOnInit() {
    if (location.pathname === '/') {
      this.beer = this.sharingData.getData();
      this.getSimilarBeers(this.beer);
    } else {
      // Part detecting we are in /detail/:id views
      const beerId = location.pathname.split('/')[2];
      this.beersService.getBeer(beerId).subscribe(
        (beer) => {
          this.beer = beer[0];
          this.getSimilarBeers(beer[0]);
        }
      );
    }
  }

  close(event) {
    this.sharingData.closeOnClick();
  }

  getSimilarBeers(beer: Beer): void {
    // it needs deeper thought to make it work without hitting server to much so I leave it as it is..
    const request: Array<string> = [];
    request[0] = `per_page=3&ibu_gt=${Math.floor(beer.ibu) + 1}`;
    request[1] = `per_page=3&iabv_gt=${Math.floor(beer.abv) + 1}`;
    request[2] = `per_page=3&ebc_gt=${Math.floor(beer.ebc) + 1}`;

    request.forEach((req) => {
      this.beersService.genericRequest(req).subscribe((response) => {
        if (response[2]) {
          this.similarBeers.push(response[2]);
        }
      });
    });
  }

  showSimilarBeerInModal(beer: Beer): void {
    this.beer = beer;
    this.similarBeers = [];
    this.getSimilarBeers(beer);
  }

}
