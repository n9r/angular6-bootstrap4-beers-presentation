import { Component, OnInit } from '@angular/core';
import { SharingService } from '../sharing-data.service';
import { Beer } from '../beer';

@Component({
  selector: 'app-beer-modal',
  templateUrl: './beer-modal.component.html',
  styleUrls: ['./beer-modal.component.css']
})

export class BeerModalComponent implements OnInit {

  beer: Beer;

  constructor(
    private sharingData: SharingService,
  ) {
    this.beer = sharingData.getData();
  }

  ngOnInit() { }

  close(event): void {
    this.sharingData.closeOnClick();
  }

}
