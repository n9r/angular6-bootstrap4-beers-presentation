import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BeersListComponent } from './beers-list/beers-list.component';
import { DetailPageComponent } from './detail-page/detail-page.component';

const routes: Routes = [
  { path: 'list', component: BeersListComponent },
  { path: '', component: BeersListComponent, pathMatch: 'full' },
  { path: 'detail/:id', component: DetailPageComponent}

];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes)]
})
export class AppRoutingModule { }
