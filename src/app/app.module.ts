import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { BeersListComponent } from './beers-list/beers-list.component';
import { BeerModalComponent } from './beer-modal/beer-modal.component';
import { SharingService } from './sharing-data.service';
import { AppRoutingModule } from './/app-routing.module';
import { DecPointDotToComaPipe } from './pipes/dec-point-dot-to-coma.pipe';
import { DetailPageComponent } from './detail-page/detail-page.component';
import { ErrorPageComponent } from './error-page/error-page.component';

@NgModule({
  declarations: [
    AppComponent,
    BeersListComponent,
    BeerModalComponent,
    DecPointDotToComaPipe,
    DetailPageComponent,
    ErrorPageComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [SharingService],
  bootstrap: [AppComponent]
})
export class AppModule { }
