import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Beer } from './beer';

@Injectable({
  providedIn: 'root'
})
export class BeersService {

  constructor(
    private http: HttpClient
  ) { }

  private beersUrl = 'https://api.punkapi.com/v2/beers';

  getBeersList(from: number, to: number): Observable<Beer[]> {

    return this.http.get<Beer[]>(`${this.beersUrl}/?page=${from}&per_page=${to}`);
  }

  getBeer(beerId) {
    return this.http.get<Beer>(`${this.beersUrl}/${beerId}`);
  }


  genericRequest(request: string) {
    return this.http.get<Beer>(`${this.beersUrl}?${request}`);
  }

}
