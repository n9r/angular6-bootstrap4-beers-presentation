import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'decPointDotToComa'
})
export class DecPointDotToComaPipe implements PipeTransform {

  transform(value: number, args?: any): string {
    return value ? value.toString().replace('.', ',') : '';
  }

}
