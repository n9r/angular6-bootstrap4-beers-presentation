import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Beer } from './beer';

@Injectable()
export class SharingService {
  private data: Beer = undefined;
  closeMessageSubject$ = new Subject<any>();

  setData(data: Beer) {
    this.data = data;
  }

  getData(): Beer {
    return this.data;
  }

  closeOnClick() {
    this.closeMessageSubject$.next('close');
  }

}
