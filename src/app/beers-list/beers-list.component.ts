import { Component, OnInit, Input } from '@angular/core';
import { SharingService } from '../sharing-data.service';

import { Beer } from '../beer';
import { BeersService } from '../beers.service';

@Component({
  selector: 'app-beers-list',
  templateUrl: './beers-list.component.html',
  styleUrls: ['./beers-list.component.css']
})
export class BeersListComponent implements OnInit {


  beersList: Array<Beer> = [];
  loadingMoreBeers = false;
  noMoreBeersToLoad = false;
  beersPerPage = 20;
  pagesLoaded = 1;
  showModalFlag = false;
  modalCurrentBeerId: number;
  errorWhileLoading = false;

  @Input() beer: Beer;
  constructor(
    private beersService: BeersService,
    private sharingData: SharingService
  ) {
    this.getMoreBeers();
  }

  ngOnInit() { }

  getMoreBeers(): void {
    this.loadingMoreBeers = true;
    this.beersService.getBeersList(
      this.pagesLoaded++, this.beersPerPage
    ).subscribe(beersList => {
      if (beersList.length) {
        this.beersList = this.beersList.concat(beersList);
        this.loadingMoreBeers = false;
      } else {
        this.noMoreBeersToLoad = true;
      }
      // AUTOMATICALLY OPENS MODAL. FOR TESTS ONLY.
      // this.showModal(this.beersList[0]);
    },
      (e) => {
        this.errorWhileLoading = true;
        this.loadingMoreBeers = false;
      }
    );
  }

  onScrollToBottom(e): void {
    // temporary solution. Doesn't work when there is no scrollbar.
    if (!this.noMoreBeersToLoad) {
      if ((window.innerHeight + window.pageYOffset) >= document.body.offsetHeight - 10) {
        this.getMoreBeers();
      }
    }
  }

  showModal(beer: Beer): void {
    this.showModalFlag = !this.showModalFlag;
    this.sharingData.setData(beer);
    this.modalCurrentBeerId = beer.id;
    this.sharingData.closeMessageSubject$.subscribe(
      (v) => {
        this.showModalFlag = false;
      },
      (e) => {
        this.errorWhileLoading = true;
        this.loadingMoreBeers = false;
      }
    );
  }

  noMoreBeers(): void {
    this.noMoreBeersToLoad = true;
  }
}
